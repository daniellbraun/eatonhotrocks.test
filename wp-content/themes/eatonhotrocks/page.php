<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context         = Timber\Timber::get_context();
$post            = new Timber\Post();
$context['post'] = $post;

$templates = array(
	'page-' . $post->post_name . '.twig',
	'page.twig',
);


/**
 * TEMPORARY SPLASH PAGE
 * To remove splash page behavior, remove this conditional
 * and template_redirect function in extras.php
 */
//if ( ! is_user_logged_in() ) {
//
//	array_unshift( $templates, 'front-page-splash.twig' );
//
//} else {

	if ( is_front_page() ) {



        $form = GFAPI::get_form( 1 );

        $context['form_title'] = $form['title'];
        $context['form_desc'] = $form['description'];

		array_unshift( $templates, 'front-page.twig' );
	}
//}

Timber\Timber::render( $templates, $context );
