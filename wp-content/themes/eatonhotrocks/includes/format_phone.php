<?php
/**
 * Strip out non-numeric characters, so phone number can be used in telephone link.
 *
 * <a href="tel:1{{ fn('format_phone', option.phone) }}">{{ option.phone }}</a>
 *
 * @param string $phone This is the non-formated phone number.
 * @return mixed
 */
function format_phone($phone) {
	return preg_replace('/[^0-9]/', '', $phone);
}