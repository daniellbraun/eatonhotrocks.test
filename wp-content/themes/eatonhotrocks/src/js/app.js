(function ($) {
    $(document).foundation();

    const links = document.querySelectorAll('[data-link]');
    links.forEach((el) => {
        el.style.cursor = "pointer";
        el.addEventListener('click', (e) => {
            window.location =  e.currentTarget.dataset.link;
        });
    });

    const prevArrow = `<button type="button" class="button slick-prev">
                        <span class="show-for-sr">Previous</span>
                        <svg class="icon"><use xlink:href="#arrow-left"></use></svg>
                    </button>`;

    const nextArrow = `<button type="button" class="button slick-next">
                        <span class="show-for-sr">Next</span>
                        <svg class="icon"><use xlink:href="#arrow-right"></use></svg>
                    </button>`;

    /*

     SPECIALS SLICK CAROUSELS

     */


    $('.specials-carousel').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        //asNavFor: '.gallery-slider',
        focusOnSelect: false,
        arrows: true,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 760,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    const frontpage_slides = document.querySelectorAll('.special-card');

    function reveal_content(e) {
        frontpage_slides.forEach((slide) => {
            if (slide != e.currentTarget) {
                slide.classList.remove('reveal-content');
            }
        });
        e.currentTarget.classList.add('reveal-content');
    }

    frontpage_slides.forEach((slide) => {
        slide.addEventListener('mouseover', reveal_content);
    });


    /*

    GALLERY SLICK CAROUSELS

     */

    $('.gallery-slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows: true,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
        fade: true,
        asNavFor: '.gallery-nav-carousel'
    });
    $('.gallery-nav-carousel').slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        asNavFor: '.gallery-slide',
        focusOnSelect: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 4
                }
            }
        ]
    });

    // Must do this for Slick Carousels inside Foudnation Modals:
    $(".gallery-modal").on("open.zf.reveal", function() {
        $(".gallery-slide").slick("setPosition", 0);
        $(".gallery-nav-carousel").slick("setPosition", 0);
    });

})(jQuery);
