(function($) {

    let page_player;

    $.createPlayer = function(player, options, buttons) {
        if (buttons) {
            buttons.addEventListener("click", (e) => {

                if (e.target && e.target.nodeName == 'BUTTON') {
                    changeVideo(e.target.dataset.video, buttons);
                }
            });

        }

        page_player = new Vimeo.Player(player, options);
        page_player.on('play', function () {
            // console.log('played the video!');
        });
    };


    function changeVideo(id, btns) {

        const buttons = btns.querySelectorAll('.video-button');

        buttons.forEach( (btn) => {
            btn.classList.contains('current-video') ? btn.classList.remove('current-video') : false;
            id === btn.dataset.video ? btn.classList.add('current-video') : false;
        } );

        page_player.loadVideo(id).then(function (id) {
            // the video successfully loaded
            page_player.play();

        }).catch(function (error) {
            switch (error.name) {
                case 'TypeError':
                    console.log("the id was not a number");
                    break;

                case 'PasswordError':
                    console.log("the video is password-protected and the viewer needs to enter the password first");
                    break;

                case 'PrivacyError':
                    console.log("the video is password-protected or private");
                    break;

                default:
                    console.log("some other error occurred");
                    break;
            }
        });
    }

})(jQuery);