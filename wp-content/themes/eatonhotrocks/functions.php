<?php
/**
 * Load all files needed for the theme.
 *
 * The $includes array determines the code library included in your theme.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$includes = [
	'includes/timber.php',       // Twig magic.
	'includes/assets.php',       // Scripts and stylesheets.
	'includes/extras.php',       // Custom functions.
	'includes/setup.php',        // Theme and widget setup.
	'includes/royalslider.php',  // RoyalSlider functions.
	'includes/format-phone.php', // That pesky format_phone function!
];

foreach ( $includes as $file ) {
	if ( ! $filepath = locate_template( $file ) ) {
		trigger_error( sprintf( 'Error locating %s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
unset( $file, $filepath );
