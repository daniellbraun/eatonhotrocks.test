<?php
/**
 * Template for displaying gallery.
 *
 * Template Name: Info
 */

$context         = Timber\Timber::get_context();
$post            = new Timber\Post();
$context['post'] = $post;

$form = GFAPI::get_form( 2 );

$context['form_title'] = $form['title'];
$context['form_desc'] = $form['description'];

Timber\Timber::render( array( 'template-info.twig', 'page.twig' ), $context );
