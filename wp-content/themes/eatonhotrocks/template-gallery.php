<?php
/**
 * Template for displaying gallery.
 *
 * Template Name: Gallery
 */

$context         = Timber\Timber::get_context();
$post            = new Timber\Post();
$context['post'] = $post;

Timber\Timber::render( array( 'template-gallery.twig', 'page.twig' ), $context );
