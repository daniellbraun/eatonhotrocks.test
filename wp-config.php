<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eatonhot_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nnpf(DoOf#Wo7j9cBQ{]<c%OAm82+ ^9Wyw63.KbmpxNx?;5fwXE1{v+U.uZ%iK/');
define('SECURE_AUTH_KEY',  '#w0X)WDN;$ [|>uLgX){I%~UTZw?L{zgZZ$1=f-Ugne%aL#G3YzMk!>VR(+}mRsu');
define('LOGGED_IN_KEY',    'EU_=Dl%VJ}w[S+_M-d=e>^GAqa;C|d!=u7=1EAn(d;8SbdYd_bMS92ct,dSCvo+B');
define('NONCE_KEY',        'kwS*Sm&j{Wb/SVrDvJ=,xqo3vHlsg}<:s?4Hh/tI0tv4)E}B-(WO|Eer@x%@*m]*');
define('AUTH_SALT',        '_TkCU<4r3}x4N)CmpmY9}phki^?VtL| 5]=ST`D`:8$^):TzkM_)Uy>Oo^~~McZj');
define('SECURE_AUTH_SALT', 'Rc)bb%wZ`o@,KKyONh7wqOSC8gS!qlO4aEnYrKH0Q]+.R;cu:HSv*@oni[0d(+s]');
define('LOGGED_IN_SALT',   '/^Bg1r|v)Cl>HcDIqz:}]i4L0h|b9f*iG@!VV8>a~kw#(>D(s5@Dr_;WJ+Y1)ovO');
define('NONCE_SALT',       'gKKKF9#LN-xS{> 6B,*Mq:rE[`I7AZvJ)@d)RdNl_HiQU3Gv*s6r9T]CgNB(-fc2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
